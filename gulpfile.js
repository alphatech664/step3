"use strict";

const gulp = require("gulp");
const { series } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const cleanCSS = require("gulp-clean-css");
const clean = require("gulp-clean");
const htmlmin = require("gulp-htmlmin");
//const imagemin = require("gulp-imagemin");
const postcss = require("gulp-postcss");
const uncss = require("postcss-uncss");
const pipeline = require("readable-stream").pipeline;

const cleanDist = function () {
    return gulp.src("dist/*", { read: false }).pipe(clean());
};

const buildHtml =  function () {
    return gulp.src('src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'))
}

const images = function () {
    return gulp
        .src("./src/img/*.+(png|jpg|jpeg|gif|svg)")
        // .pipe(
        //     imagemin({
        //         interlaced: true,
        //     })
        // )
        .pipe(gulp.dest("dist/img"));
};

const buildSass = function () {
    return gulp
        .src("src/*.scss")
        .pipe(sass())
        .pipe(concat("styles.min.css"))
        .pipe(autoprefixer(["> 0.1%"], { cascade: true }))
        .pipe(gulp.dest("./dist"))
        .pipe(browserSync.stream());
};

const buildJs = function () {
    return gulp
        .src("src/js/*.js")
        .pipe(gulp.dest("./dist/js"));
};
const buildScriptJs = function () {
    return gulp
        .src("src/*.js")
        .pipe(gulp.dest("./dist"));
};

const minifyCss = function () {
    return gulp
        .src("dist/*.css")
        .pipe(cleanCSS({ compatibility: "ie8" }))
        .pipe(gulp.dest("./dist"));
};

gulp.task("dev", function () {
    browserSync.init({
        server: "./dist",
    });
    gulp.watch("src/*.scss", buildSass);
    gulp.watch("src/*.html").on("change", browserSync.reload);
    gulp.watch("src/**/*.js", buildJs);
});

gulp.task("build", series(cleanDist,buildHtml, buildSass, minifyCss,buildScriptJs, buildJs, images));
