import Api from "./js/api.js";
import Modal from "./js/modal.js";
import NewAdminForm from "./js/admin-form.js";
import VisitForm from "./js/visitm-form.js";
import FormDentist from "./js/form-dentist.js";
import FormTherapist from "./js/form-therapist.js";
import FormCardiologist from "./js/form-cardiologist.js";
import Element from "./js/element.js";
import UserCard from "./js/card.js";



const url_base = 'https://ajax.test-danit.com/api/v2/cards/'
const api = new Api(url_base)
const token = api.isToken()


const btnEnter = document.querySelector('.enter');
btnEnter.addEventListener('click', listner)


async function listner(){
    console.log('click')
    let form = new NewAdminForm('form','forma')
    console.log(form)
    let m0dal = new Modal('div','modal-dialog',{headerTitle: 'Enter login end password', body: ''});
    let mod_body = document.querySelector('.modal-body')
    mod_body.append(form.element)
    btnEnter.removeEventListener('click', listner)

    document.querySelector('.forma').addEventListener('submit',async (event)=>{
        event.preventDefault()

        let token = await form.sendFormData()
        console.log(token)
        let data = await form.getUsersData(token)
        console.log(data)
        if (data.length > 1){
            document.querySelector('.text').remove()

            data.forEach(user => {
                let {age,description,doctor,diseases,fullName,id,lastVisit,presure,title,urgency} =user
                const card = new UserCard('div','card',fullName,doctor[0].toUpperCase()+ doctor.slice(1),id,title,description,diseases,age,presure,lastVisit,urgency)
                document.querySelector('.card-body').append(card.element)
            });

        }else {
            const div = new Element('div','text')
            div.element.innerHTML = 'No items have been added'
            document.querySelector('.card-body').append(div.element)
        }

    })

    document.querySelector('.btn').addEventListener('click', (event)=>{
        console.log('click')
        btnEnter.setAttribute("disabled", "disabled")

        const m0da1 = new Modal('div','modal-dialog',{headerTitle: 'Create a Visit', body: ''});
        let visit_form = new VisitForm('div','form-group')
        document.querySelector('.modal-body').append(visit_form.element)

        let obj = {dentist: new FormDentist('div', 'form-g'),therapist: new FormTherapist('div','form-g'),cardiologist: new FormCardiologist('div', 'form-g') }
        visit_form.sendFormData(obj)

        document.querySelector('.form-control').addEventListener('change',(event)=>{
            let formname = document.querySelector('.form-control').value

            if(document.querySelector('.form-g') == null){
                document.querySelector('.modal-body').append(obj[formname].element)

            }else {
                document.querySelector('.form-g').remove()
                document.querySelector('.modal-body').append(obj[formname].element)
            }
            if(document.querySelector('.form-g')){
                let children = document.querySelector('.form-g').children
                let obj = {
                    title: '',
                    description: '',
                    doctor: `${formname}`,
                    diseases: '',
                    age:'',
                    weight: '',
                    fullName: '',
                    lastVisit: '',
                    presure: '',
                    urgency: ''
                }

                document.querySelector('.form-g').querySelector('.btn').addEventListener('click',async (event)=>{
                    console.log('click')

                    for(let i=0; i< children.length; i++){
                        let input = children[i].querySelector('.form-control')
                        if(input){
                            console.log(input)
                            if(input.classList.contains('urgency')){
                                obj.urgency = input.value
                            }

                            if(input.classList.contains('purpose-visit') ){
                                obj.title = input.value

                            }
                            else if (input.classList.contains('description') ){
                                obj.description = input.value
                            }

                            else if (input.classList.contains('full-name')){
                                obj.fullName = input.value
                                console.log(input.value)
                            }
                            else if (input.classList.contains('age')){
                                obj.age = input.value
                            }
                            else if (input.classList.contains('last-visit')){
                                obj.lastVisit = input.value
                            }
                            else if(input.classList.contains('normal-presure')){
                                obj.presure = input.value
                            }

                            else if(input.classList.contains('diseases')){
                                obj.diseases = input.value
                            }
                        }


                    }
                    const api = new Api(url_base)
                    const token = api.isToken()

                    await fetch("https://ajax.test-danit.com/api/v2/cards", {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify(obj)
                    })
                        .then(response => response.json())
                        .then(response => {

                            let {age,description,doctor,diseases,fullName,id,lastVisit,presure,title,urgency} = response;
                            //console.log(urgency)
                            if(document.querySelector('.text')){
                                document.querySelector('.text').remove()
                            }

                            const card = new UserCard('div','card',fullName,doctor[0].toUpperCase()+ doctor.slice(1),id,title,description,diseases,age,presure,lastVisit,urgency)
                            document.querySelector('.card-body').append(card.element)



                        })

                })

            }

        })

    })

    console.log('1')
}

const childs = document.querySelector('.card-body').children
document.querySelector('.doctor-control').addEventListener('change', (event)=>{
    let value= document.querySelector('.doctor-control').value
    console.log(value)
    if(childs.length >0){
        for(let i = 0; i< childs.length;i++){
            if(childs[i].classList.contains(`${value}`)){
                console.log(childs[i])
                childs[i].classList.toggle('show')
                childs[i].classList.remove('hide')
            }else{
                childs[i].classList.remove('show')
                childs[i].classList.add('hide')
            }

        }
    }

})

document.querySelector('.pressure-control').addEventListener('change', (event)=>{
    let pressure_control = document.querySelector('.pressure-control').value
    console.log(pressure_control)
    for(let i = 0; i< childs.length;i++){
        if(childs[i].classList.contains(`${pressure_control}`)){
            childs[i].classList.toggle('show')
            childs[i].classList.remove('hide')
        }
        else{
            childs[i].classList.remove('show')
            childs[i].classList.add('hide')
        }
    }

})

document.querySelector('.element-search').addEventListener('click', (event)=>{
    event.preventDefault()
    let value =  document.querySelector('#card-search').value
    for(let i = 0; i< childs.length;i++){
        childs[i].classList.remove('show')
        if(value == childs[i].querySelector('.title').innerHTML ){
            childs[i].classList.add('show')
        } else {
            childs[i].classList.add('hide')
        }
    }
})