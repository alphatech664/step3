import Api from "./api.js";
import Element from "./element.js";
import UserCard from "./card.js";

class NewAdminForm extends Element {
    constructor(element,class_) {
        super(element,class_)
        this.setInnerHTML()
        //this.sendFormData()
        //this.getUsersData()
        }
        setInnerHTML(){
        return this.element.insertAdjacentHTML('beforeend',`
      <div class="form-signup">
             <label for="email">Email</label>
             <input id="login" class="form-control" name="login" placeholder="Email">
             </div>
             <div class="form-signup">
             <label for="password">Password</label>
             <input id="password" class="form-control" name="password" placeholder="Password">
         </div>
              <button type="submit" class="btn btn-primary">Sign Up</button>
      `)
        }

        async sendFormData(){
          let token;
          console.log(this.element)
        
              const newAdmin = { 
                  email: this.element.login.value,
                  password: this.element.password.value,
              }
  
    
        let request = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
          method: 'POST',
          headers: {
          'Content-Type': 'application/json'
            },
          body: JSON.stringify(newAdmin)
          })
          console.log(request)
          let response = await request.text()
          token = response
          localStorage.setItem('token', JSON.stringify(response))
          //console.log(response)
     
          document.querySelector('.modal-dialog').remove()
          document.querySelector('.btn').innerHTML = 'Створити візит'
          document.querySelector('.btn').classList.add('create')  
        
       
        return token
       } 
        async getUsersData(token){
        let request = await fetch("https://ajax.test-danit.com/api/v2/cards/", {
          method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
          }
        })

          let response = await request.json();
          return response
       }   
     
    
}
export default NewAdminForm
