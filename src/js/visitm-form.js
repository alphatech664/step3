import Element from "./element.js";
import UserCard from "./card.js";
import Api from "./api.js";
class VisitForm extends Element{
    constructor(element,class_,bodyFormDoctor){
        super(element,class_)
        this.bodyFormDoctor = bodyFormDoctor
        this.setInnerHTML()
        //this.sendFormData()
       // this.createCard()
    }

    setInnerHTML(){
        return this.element.insertAdjacentHTML('beforeend',`
    <label for="exampleFormControlSelect1">Сhoose a doctor</label>
    <select class="form-control" >
    <option value="cardiologist">Сardiologist</option>
    <option value="dentist" selected>Dentist</option>
    <option value="therapist">Therapist</option>
    </select>
    <div class="form-doctor"></div>
      `)
    }

     async sendFormData(some_obj){
       
        this.element.querySelector('.form-control').addEventListener('change',(event)=>{
        let formname = this.element.querySelector('.form-control').value
        console.log(formname)
        console.log(document.querySelector('.modal-body'))
        if(document.querySelector('.form-g') == null){
          this.element.append(some_obj[formname].element)

        }else{
          document.querySelector('.form-g').remove()
          this.element.append(some_obj[formname].element)
       } 
      
      }) 

    } 

     
}

export default VisitForm