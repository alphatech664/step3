import Element from "./element.js";
//import Modal from "./modal.js"

class Modal extends Element{
    constructor(element,class_,{headerTitle, body}){
        super(element,class_)
        this.headerTitle = headerTitle
        this.body = body
        this.setInnerHTML()
        this.render()
        this.close()
    }
    setInnerHTML(){

        return this.element.insertAdjacentHTML('afterbegin',`
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">${this.headerTitle}</h5>
                        <button type="button" class="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                      </div>
                </div>
           
        `)}

    render(){
        document.body.append(this.element)
    }
    close(){
        this.element.querySelector('.close').addEventListener('click',(event)=>{this.element.remove()
        document.querySelector('.enter').removeAttribute('disabled')
        
        })
    }
    add(some_element){
        this.element.insertAdjacentElement('beforeend',some_element);
    }


}

export default Modal