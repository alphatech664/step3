class Api {
  constructor(baseUrl){
  this.token = '';
  this.baseUrl = baseUrl;
    }
  
    async get(cardId){
     let response = await fetch(`${this.baseUrl}${cardId}`, {
      method: 'GET',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.token}`
      }
    })
     let data = await response.json()
     console.log(data)
     return data
      
    }

    async getAllCards(){
      let response = await fetch(`${this.baseUrl}`, {
       method: 'GET',
       headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.token}`
       }
     })
      let data = await response.json()
      console.log(data)
      return data
       
     }
  
  async post(data,url=''){
  let response = await fetch(`${this.baseUrl}${url}`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
    },
    body: JSON.stringify(data)
  }) 
  let token = await response.text()
  
  this.token = token
  
  localStorage.setItem('token', JSON.stringify(this.token))
  return this.token
    }
  
  isToken(){
  const token = JSON.parse(localStorage.getItem('token'))
  if(token){
  this.token =token
  }
  return this.token
  }
  ///////////
  
  
  
  
  
  ///////////
  }
  
  export default  Api