import Element from "./element.js";
class FormCardiologist extends Element {
    constructor(element,class_) {
        super(element,class_)
        this.setInnerHTML()
        //this.getData()
    }
    setInnerHTML(){
        return this.element.insertAdjacentHTML('beforeend',
              `
         <div class="form-group">
                         <input class="form-control purpose-visit" name="purpose-visit" placeholder="Ціль візиту" value="">
             </div>
             <div class="form-group">
   
    <textarea class="form-control description" name="description" placeholder="Короткий опис візиту"  rows="3" value=""></textarea>
    </div>
    <div class="form-group">
    <div>Терміновість візиту</div>
    <select class="form-control urgency">
    <option value="high">Високий</option>
    <option value="normal" >Нормальний</option>
    <option value="low">Низький</option>
    </select>
    </div>
                <div class="form-group">
                            <input class="form-control full-name" name="full-name" placeholder="ПІП" value="">
                </div>

                <div class="form-group">
                            <input  class="form-control normal-pressure" name="normal-pressure" placeholder="Звичайний тиск" value="">
                </div>

                <div class="form-group">
                            <input  class="form-control diseases" name="diseases" placeholder="Перенесені захворювання серцево-судинної системи" value="">
   
                            </div>
                <div class="form-group">
                            <input  class="form-control age" name="age" placeholder="Вік" value="">
                </div>
                <button type="submit" class="btn btn-primary">Записати візит</button>    `


        )
    }

   /*  getData(){
        console.log(this.element)
        this.element.querySelector('.btn').addEventListener('click', (event)=>{
            console.log(this.element)
            console.log(this.element.diseases.value)
        })
    } */

}

export default FormCardiologist