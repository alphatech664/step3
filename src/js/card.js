import Element from './element.js';
import Api from './api.js';
import VisitForm from "./visitm-form.js";
import Modal from "./modal.js"; import FormDentist from "./form-dentist.js";
import FormCardiologist from "./form-cardiologist.js";
import FormTherapist from "./form-therapist.js"; 


class UserCard extends Element {
  constructor(element,class_,user_name, doctor, id,title,description,diseases,age,presure,lastVisit,urgency){
    super(element,class_)
    this.user_name = user_name;
    this.doctor = doctor;
    this.id = id;
    this.title = title;
    this.description = description;
    this.diseases = diseases;
    this.age = age;
    this.presure=presure;
    this.lastVisit =lastVisit;
    this.urgency = urgency;
    this.renderHeader()
    this.setAtribute()
    this.setClassList()
    this.renderBody() 
  }

  renderHeader(){
    this.element.insertAdjacentHTML("afterbegin", `<div class="card-header"><h3 class="card-inf doctor">${this.doctor}</h3><br><h4 class="card-inf full-name">${this.user_name}</h4></div>     <div><button class="btn btn-primary" type="button">show more</button></div>`)
    this.element.insertAdjacentHTML('beforeend', '<svg class="icon-del" fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="24px" height="24px"><path d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"/></svg>')
   
    this.element.insertAdjacentHTML('beforeend', `<svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class=" icon-add-task bi bi-calendar-plus" viewBox="0 0 16 16">
    <path d="M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z"/>
    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
    </svg>`)


     this.element.addEventListener('click', (e) => {
      let api =  new Api('https://ajax.test-danit.com/api/v2/cards/')
      const token = api.isToken()
      const delTarget = e.target.closest('.icon-del');
      const btnTarget = e.target.closest('.btn')
      const corectTarget = e.target.closest('.icon-add-task')
      console.log(e.target)

        if (delTarget) {
            const toDelUser = confirm('Delete User?');
            if (toDelUser) {
                this.element.remove();
                fetch(`${'https://ajax.test-danit.com/api/v2/cards/'}${this.id}`, {
                      method: 'DELETE',
                      headers: {
                      'Content-Type': 'application/json',
                      'Authorization': `Bearer ${token}`
                      }

                  })
            }

          }
              
              
          if(corectTarget){
            fetch(`${'https://ajax.test-danit.com/api/v2/cards/'}${this.id}`, {
                method: 'GET',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                }
            })
              .then(response => response.json())
              .then(response =>{
              
                let {age,description,doctor,diseases,fullName,id,lastVisit,presure,title,urgency} = response;
                if(document.querySelector('.modal-dialog')== null){
                const m0da1 = new Modal('div','modal-dialog',{headerTitle: 'Create a Visit', body: ''});
                let visit_form = new VisitForm('div','form-group')
                document.querySelector('.modal-body').append(visit_form.element)
  
                let obj = {dentist: new FormDentist('div', 'form-g'),therapist: new FormTherapist('div','form-g'),cardiologist: new FormCardiologist('div', 'form-g') }
                
                document.querySelector('.form-control').addEventListener('change',(event)=>{
                  let formname = document.querySelector('.form-control').value
                  response.doctor = formname
                  if(document.querySelector('.form-g') == null){
                    document.querySelector('.modal-body').append(obj[doctor].element)
                        console.log(obj[formname].element)
                        let children =  document.querySelector('.form-g').children
                          /////////
                    for(let i=0; i< children.length; i++){

                      let input = children[i].querySelector('.form-control')
                      if(input){

                        if(input.classList.contains('purpose-visit') ){
                           input.value = title;
                        }
                         else if(input.classList.contains('urgency')){
                          input.value = urgency;
                        }

                        else if (input.classList.contains('description') ){
                           input.value = description;
                        }
              
                        else if (input.classList.contains('full-name')){
                          input.value = fullName;
                        }
                        else if (input.classList.contains('age')){
                          input.value = age;
                        }
                        else if (input.classList.contains('last-visit')){
                          input.value = lastVisit;
                        }
                        else if(input.classList.contains('normal-presure')){
                          input.value = presure;
                        }
              
                        else if(input.classList.contains('diseases')){
                          input.value = diseases;
                        }
                     
                      }

                    }  

                  } else {
                      document.querySelector('.form-g').remove()
                      document.querySelector('.modal-body').append(obj[formname].element)
                    }
                    
                    let children =  document.querySelector('.form-g').children;

                    for(let i=0; i< children.length; i++){
                      let input = children[i].querySelector('.form-control')
                      if(input){
                        input.addEventListener('change',(event)=>{
                        console.log(input.value)
                          if(input.classList.contains('purpose-visit') ){
                            response.title = input.value;
                          }
                          else if(input.classList.contains('urgency') ){
                            response.urgency = input.value;
                          }

                          else if(input.classList.contains('description') ){
                            response.description = input.value;
                          }
                          else if (input.classList.contains('full-name')){
                            response.fullName = input.value;
                          }
                          else if (input.classList.contains('age')){
                            response.age = input.value;
                          }
                          else if (input.classList.contains('last-visit')){
                            response.lastVisit = input.value;
                          }
                          else if(input.classList.contains('normal-presure')){
                            response.presure = input.value;
                          }
                          else if(input.classList.contains('diseases')){
                            response.diseases = input.value;
                          }
                                                    
                            document.querySelector('.form-g').querySelector('.btn').addEventListener('click', (event)=>{
                                console.log('clikkkkk')
                              fetch(`${'https://ajax.test-danit.com/api/v2/cards/'}${this.id}`, {
                                method: 'PUT',
                                headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${token}`
                                  },
                                body: JSON.stringify(response)
                             })
                                .then(data => data.json())
                                .then(data => {
                
                              let children = document.querySelector('.card[data-cards="'+this.id+'"]').querySelector('.card-header').children
                               for(let i=0; i<children.length; i++){
                                if(children[i].classList.contains('card-inf')){
                                if(children[i].classList.contains('doctor')){
                                  children[i].innerHTML = response.doctor[0].toUpperCase()+ response.doctor.slice(1)
                                }else if (children[i].classList.contains('full-name')){                                 
                                    children[i].innerText = response.fullName
                                    } 
                                 
                                }
                              }

                              let body_childs =document.querySelector('.card[data-cards="'+this.id+'"]').querySelector('.body').children
                              for(let i=0; i<body_childs.length; i++){
                                if(body_childs[i].classList.contains('card-inf')){
                          
                                  if(body_childs[i].classList.contains('title')){
                                    
                                    body_childs[i].innerHTML = response.title
                                  }
                                  else if (body_childs[i].classList.contains('description')){
                            
                                    body_childs[i].innerHTML = response.description
                                
                                  }else if (body_childs[i].classList.contains('diseases')){
                                  
                                  body_childs[i].innerHTML = response.diseases
                                  }else if (body_childs[i].classList.contains('age')){
                                  
                                  body_childs[i].innerHTML = response.age
                                 
                                  }
                                  else if (body_childs[i].classList.contains('presure')){
                                
                                  body_childs[i].innerHTML = response.presure
                                  }
                                  else if (body_childs[i].classList.contains('lastVisit')){
                          
                                  body_childs[i].innerHTML = response.lastVisit
                                  }
                                  else if (body_childs[i].classList.contains('urgency')){
                          
                                    body_childs[i].innerHTML = response.urgency
                                    }
                                }
                              }

                              })
                           })

                        })


                      }

                        
                    }
                      
              
                })
                }
              })
            //
     

          }
         if(!btnTarget.classList.contains('show-body')){

             btnTarget.classList.add('show-body')
             this.element.querySelector('.body').classList.add('show')
             btnTarget.innerHTML = 'hide'
         }else {
             this.element.querySelector('.body').classList.remove('show')
             btnTarget.textContent = 'show more'
             btnTarget.classList.remove('show-body')

         }
    
    }) 

    return this.element
  }
    renderBody(){
    return this.element.insertAdjacentHTML('beforeend', `<div class="body hide"><h5 class="card-inf title">${this.title}</h5><br><h5 class="card-inf descript"> ${this.description}</h5><br><h5 class="card-inf diseases">${this.diseases}</h5><h5 class="card-inf age">${this.age}</h5><h5 class="card-inf presure">${this.presure}</h5><h5 class="card-inf lastVisit">${this.lastVisit}</h5><h5 class="card-inf urgency">${this.urgency}</h5></div>`)
   }

   setAtribute(){
    return this.element.setAttribute('data-cards', `${this.id}`)
   }
   setClassList(){
    this.element.classList.add(`${this.doctor[0].toLowerCase()+ this.doctor.slice(1)}`)
    this.element.classList.add(`${this.urgency}`)
    this.element.classList.add(`all`)
   return this.element
   } 

}
export default UserCard
